ARG NODE_VERSION=20
FROM docker.io/node:${NODE_VERSION}-alpine AS builder

ENV NODE_ENV=development

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm clean-install

COPY src/ src/
RUN npm run build

FROM docker.io/node:${NODE_VERSION}-alpine

ARG PORT=2080
EXPOSE ${PORT}
WORKDIR /usr/src/app
VOLUME ["/usr/src/app/db"]

ENV NODE_ENV=production
ENV PORT=${PORT}

# only production dependencies needed here
COPY package*.json ./
RUN npm clean-install --only=production

COPY --from=builder /usr/src/app/build/ ./build/
CMD ["npm", "run", "start"]
