<div align="center">
<img src="assets/logo/logo.svg" width="128px" height="128px" alt="Mezzo logo" />

# Mezzo

A simple Single Sign-On provider to go in-between all your subdomains.

</div>

## About

Mezzo is a SSO provider for a simple case where all websites are subdomains of a single main domain. For example, Mezzo works great for websites of the pattern `*.example.com`. Its main goals are to be simple, secure, and easy to use for self-hosted services. For example, there is _no_ client-side JavaScript and it is explicitly forbidden to contain any with a Content Security Policy (CSP).

Mezzo is self contained without requiring any other services. It provides its own user management with admin and normal users. Mezzo contains Role Based Access Control (RBAC) for subdomain rules.

This is not meant for enterprise situations and that is the biggest feature. It is meant for simple setup where admins don't have to configure many services to get up and running. For example, Mezzo does not support LDAP or any other external user management. It also doesn't require setting up a database, all data is stored in a simple JSON file.

**Word of caution: Although I believe this is reasonably secure, this was developed in my free time as a hobby project and there has not been any professional security review done of the code.**

That said, please report any security issues found.

Some inspiration was taken from [Authelia](https://www.authelia.com/), and you should take a look at that as something that provides a similar service but is also more supported.

### Features

- Easy to setup and run (via Docker)
- No client-side JavaScript
- No database required (only a single JSON file)
- Simple to configure
    - You can directly edit the JSON file if needed
    - Web UI for managing users and access control
- Can use with a reverse proxy to authenticate requests
- Less than 2500 lines of code, including comments and HTML templates

### API

**GET `/api/verify`** - Checks if a user has access to a URL.

Query parameters:

- `url`: (optional) The URL to check against the access control rules. If provided, it overrides the `X-Forwarded-Host`, `X-Forwarded-Proto`, and `X-Forwarded-Uri` headers.

HTTP headers:

- `X-Forwarded-Host`, `X-Forwarded-Proto`, `X-Forwarded-Uri`: The URL to check against the access control rules
- `Authorization`: The user token in the format `Bearer ${TOKEN}`
- `Cookie`: The user token in the format of `token=${TOKEN}`

Note: At least one of `Authorization` or `Cookie` is required to supply the user token.

A full example url for the api call to verify the URL "https://portal.example.com/page1" could look like `/api/verify?url=https://portal.example.com/page1`.

## Setup

The "database" needs to be seeded manually with a JSON file. First, create a `db` folder with a `mezzo.json` file inside. This file will also get created automatically the first time it is run, but it will have blank/default values that need to be changed. That file contains `domain`, `users`, and `access_control` that need to be filled in.

- `domain` is the base domain used for all other subdomains, such as `example.com`.
    - This is also used for the cookie set when signing in so that it can be shared across all subdomains.
- `users` is an object of users that contains username, password, time zone and roles.
    - At least one admin user is required to be setup, i.e. `roles` must contain `admin`.
    - The password is hashed using [Argon2](https://en.wikipedia.org/wiki/Argon2).
    - The time zone is used for displaying dates and times and is in the format of `America/Los_Angeles`. A list can be found here [List_of_tz_database_time_zones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones).
- `access_control` is an object of RBAC rules for each subdomain. It contains the rules for who has access to sites.
    - The keys are the subdomains, or the special key `*` is a fallback if the domain is not found.
    - Each rule is an object specifying the type and roles.
    - `type` is either `allow` or `deny`.
    - `roles` is an array of roles to either allow (allow only these roles, deny all others) or deny (deny only these roles, allow all others).
    - As of now, there is no way to specify public access to sites. All sites require at least a user to be signed in before they can access it.

A template is below that is setup for `*.example.com` domains, has a single admin user `my_username` with password `my_password`, and denies all sites to all users. Once setup, you can change the password of the user from the web interface.

```json
{
    "domain": "example.com",
    "mezzo_url": "https://mezzo.example.com",
    "users": {
        "my_username": {
            "username": "my_username",
            "password": "$argon2id$v=19$m=4096,t=3,p=1$bwNEv1gb5RbN9+8m/E+5cw$OYE1bfYQmq0lNCG8rW6wvabBs1nCb4OhkCNpVRXuOtA",
            "timeZone": "UTC",
            "roles": ["admin"]
        }
    },
    "access_control": {
        "*": {
            "type": "allow",
            "roles": []
        }
    }
}
```

Notes:

- To generate a password hash you can use `echo -n 'my_password' | npx argon2-cli -id`.
    - Or if you don't want the password in the shell history: `cat | tr -d '\n' | npx argon2-cli -id`, type the password and press ctrl-d
- The role `admin` gives the user the ability to view/add/edit/delete other users and manage the access control rules.

## Run

The default port it will run on is 2080 but can be changed with the environment variable `PORT`.

### Manually

You can run Mezzo manually with Node.js by cloning the repository, installing dependencies, and running the server.

```bash
git clone https://gitlab.com/janka102/mezzo.git
cd mezzo
npm install

npm start

# or with custom port
PORT=3080 npm start
```

### Docker

You can download a docker image and run that. The latest image is located at: `registry.gitlab.com/janka102/mezzo:latest`

You will need to attach a volume to `/usr/src/app/db` to persist the database.

```bash
docker run -it --rm -v ./db:/usr/src/app/db -p 2080:2080 registry.gitlab.com/janka102/mezzo:latest
```

#### Docker Compose

You can also use docker compose to run Mezzo.

```yaml
services:
    mezzo:
        image: registry.gitlab.com/janka102/mezzo:latest
        ports:
            - 2080:2080
        volumes:
            - ./db:/usr/src/app/db
```

## Use in a reverse proxy

### Caddy

Here is an example Caddy configuration. Caddy automatically adds the needed headers.

```caddyfile
# Serve Mezzo portal
mezzo.example.com {
    reverse_proxy http://localhost:2080
}

# Something that requires authentication
something.example.com {
    forward_auth http://localhost:2080 {
        uri /api/verify
        copy_headers X-Remote-User X-Remote-Groups
    }

    reverse_proxy http://localhost:5000
}
```
