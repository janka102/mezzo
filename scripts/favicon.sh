#!/usr/bin/env bash
# shellcheck enable=all disable=SC2016

# A script to convert a large image into smaller files used for "favicon.ico".
# See [favicon-cheat-sheet](https://github.com/audreyfeldroy/favicon-cheat-sheet) for details on which sizes are best.
#
# NOTE: GraphicsMagick doesn't support writing .ico files,
# see http://www.graphicsmagick.org/formats.html

set -euo pipefail
# set -x

function main() {
    if [[ "${1:-}" == '-h' ]] || [[ "${1:-}" == '--help' ]]; then
        usage "$@"
    fi

    local self_dir assets_dir image magick_cmd

    self_dir="$(dirname "$0")"
    assets_dir="$(realpath --relative-to=. "${self_dir}/../assets")"

    image="${1:-"${assets_dir}/logo/logo.svg"}"

    if command -v magick > /dev/null 2>&1; then
        magick_cmd='magick'
    elif command -v convert > /dev/null 2>&1; then
        magick_cmd='convert'
    else
        echo 'Cannot find ImageMagick'
        exit 1
    fi

    if ! command -v optipng > /dev/null 2>&1; then
        echo 'Cannot find OptiPNG'
        exit 1
    fi

    echo "Scaling ${image}..."
    cd "$(dirname "${image}")"
    image="$(basename "${image}")"

    for size in 48 32 16; do
        "${magick_cmd}" "${image}" -scale "${size}x${size}" -background none "favicon-${size}.png"
    done

    echo 'Optimizing...'
    for size in 48 32 16; do
        optipng -q -o7 "favicon-${size}.png"
    done

    echo 'Making ico...'
    "${magick_cmd}" favicon-48.png favicon-32.png favicon-16.png favicon.ico
}

function usage() {
    local self_dir assets_dir default_image

    self_dir="$(dirname "$0")"
    assets_dir="$(realpath --relative-to=. "${self_dir}/../assets")"
    default_image="${assets_dir}/logo/logo.svg"

    echo 'Convert a large image into smaller files used for "favicon.ico"'
    echo "Usage: $(basename "$0") [OPTIONS] [IMAGE_FILE]"
    echo
    echo 'Options:'
    echo '    -h, --help   Show this help message'
    echo
    echo 'Arguments:'
    echo "    IMAGE_FILE   Input image file (default: ${default_image})"
    echo
    echo 'Dependencies:'
    echo '    - ImageMagick (`convert` or `magick` command)'
    echo '    - OptiPNG (`optipng` command)'
    exit 0
}

main "$@"
