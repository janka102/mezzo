import fs from "fs"
import path from "path"
import { Low } from "lowdb"
import { DataFile } from "lowdb/node"
import lodash, { type ObjectChain } from "lodash"

const DB_DIR = path.resolve(process.cwd(), "db")
const DB_NAME = "mezzo.json"
const DB_PATH = path.resolve(DB_DIR, DB_NAME)

if (!fs.existsSync(DB_DIR)) {
    fs.mkdirSync(DB_DIR)
}

export type User = {
    username: string
    roles: string[]
    timeZone: string
    password: string
}

export type Token = {
    value: string
    username: string
    userAgent: string
    key: string
    created: number
    expires: number
}

export type AccessControl = {
    type: "allow" | "deny"
    roles: string[]
}

export enum FlashType {
    DANGER = "danger",
    WARNING = "warning",
    INFO = "info",
    SUCCESS = "success",
}
export type Flash = Partial<Record<FlashType, string[]>>

export type Database = {
    domain: string
    mezzo_url: string
    users: Record<string, User>
    tokens: Record<string, Token>
    access_control: Record<string, AccessControl>
    flashes: Record<string, Flash>
}

class LodashAdapter<T extends {}> extends DataFile<ObjectChain<T>> {
    constructor(filename: fs.PathLike) {
        super(filename, {
            parse: LodashAdapter.#parse,
            stringify: LodashAdapter.#stringify,
        })
    }

    static #parse<T extends {}>(str: string): ObjectChain<T> {
        const data: T = str ? JSON.parse(str, LodashAdapter.#reviver) : {}
        return lodash.chain(data)
    }

    /** Make sure objects have null prototype when JSON.parse is used. */
    static #reviver<T>(_key: string, value: T): T {
        if (value && typeof value === "object" && !Array.isArray(value)) {
            return Object.assign(Object.create(null), value)
        }
        return value
    }

    static #stringify<T extends {}>(data: ObjectChain<T>): string {
        return JSON.stringify(data.value(), null, 2)
    }
}

const DEFAULTS: () => Database = () => ({
    domain: "",
    mezzo_url: "",
    users: Object.create(null),
    tokens: Object.create(null),
    access_control: Object.create(null),
    flashes: Object.create(null),
})
const adapter = new LodashAdapter<Database>(DB_PATH)
const db = new Low(adapter, lodash.chain(DEFAULTS()))

export default db.read().then(async () => {
    db.data.defaults(DEFAULTS()).value()

    const errors = []

    if (!db.data.get("domain").value()) {
        errors.push("Missing domain in the db.")
    } else {
        db.data.set("domain", db.data.get("domain").value().toLowerCase())
    }

    if (!db.data.get("mezzo_url").value()) {
        errors.push("Missing mezzo_url in the db.")
    } else {
        try {
            const domain = db.data.get("domain").value()
            const mezzoUrl = new URL(db.data.get("mezzo_url").value())
            if (
                mezzoUrl.hostname !== domain &&
                !mezzoUrl.hostname.endsWith(`.${domain}`)
            ) {
                errors.push("mezzo_url must end with the domain")
            }
        } catch (e) {
            errors.push("Invalid mezzo_url in the db.")
        }
    }

    if (
        !db.data
            .get("users")
            .some({ roles: ["admin"] })
            .value()
    ) {
        errors.push("Missing admin user in the db.")
    }

    await db.write()

    if (errors.length) {
        throw new Error(errors.join(" "))
    }

    return db
})
