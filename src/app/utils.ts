import dbp from "./db.js"

/**
 * Delete all expired tokens from the database.
 * Also queue another call to this function for a later time.
 */
async function removeExpiredTokens() {
    const db = await dbp
    const now = Date.now()
    const tokens = db.data
        .get("tokens")
        .omitBy(({ expires }) => expires <= now)
        .value()

    db.data.set("tokens", tokens).value()
    await db.write()

    // queue another removal
    const minDelay = 10 * 60 * 1000
    const jitter = 5 * 60 * 1000
    const delay = Math.floor(minDelay + Math.random() * jitter)
    setTimeout(removeExpiredTokens, delay)
}

/** If a value is not a string, return an empty string. */
function asString(value: unknown): string {
    return typeof value === "string" ? value : ""
}

function getValidTimeZones(): string[] {
    return Intl.supportedValuesOf("timeZone").concat("UTC")
}

export { asString, getValidTimeZones, removeExpiredTokens }
