import bodyParser from "body-parser"
import cookieParser from "cookie-parser"
import express from "express"
import helmet from "helmet"
import morgan from "morgan"
import nunjucks from "nunjucks"
import path from "path"

import type { FlashType } from "./db.js"
import { flash } from "./flash.js"
import router from "./router/router.js"
import { removeExpiredTokens } from "./utils.js"

const __dirname = import.meta.dirname
const development =
    !process.env.NODE_ENV || process.env.NODE_ENV === "development"

const PORT = process.env.PORT || "2080"
const TRUST_PROXY = process.env.TRUST_PROXY

function main() {
    const app = express()

    const trustProxy = TRUST_PROXY ? JSON.parse(TRUST_PROXY) : null
    if (trustProxy != null) {
        app.set("trust proxy", trustProxy)
    }

    app.use(morgan(development ? "dev" : "combined"))
    app.use(
        helmet({
            contentSecurityPolicy: {
                directives: {
                    // Disable upgradeInsecureRequests in development b/c WebKit tries to upgrade localhost to https
                    upgradeInsecureRequests: development ? null : [],

                    // Forbid any client-side JavaScript
                    "script-src": ["'none'"],
                },
            },
        }),
    )

    app.use(express.static(path.resolve(__dirname, "../public")))

    // use "views" folder to look for templates
    const nunjucksEnv = nunjucks.configure(
        path.resolve(__dirname, "../views"),
        {
            noCache: development,
            watch: development,
            trimBlocks: true,
            lstripBlocks: true,
            express: app,
        },
    )
    // use ".html" extension for templates
    app.set("view engine", "html")

    nunjucksEnv.addFilter("alert_icon", (type: string) => {
        const icons: Record<FlashType, string> = {
            danger: "exclamation-circle",
            warning: "exclamation-triangle",
            info: "info-circle",
            success: "check-circle",
        }

        return type in icons
            ? `<i class="fas fa-${icons[type as FlashType]}"></i>`
            : ""
    })

    nunjucksEnv.addFilter(
        "await",
        async (value, cb) => {
            try {
                cb(null, await value)
            } catch (e) {
                cb(e)
            }
        },
        true,
    )

    app.use(bodyParser.urlencoded({ extended: false }))
    app.use(cookieParser())
    app.use(flash())
    app.use(router)

    // Start the process to remove expired tokens every so often
    removeExpiredTokens()

    const server = app.listen(PORT, () => {
        const addrInfo = server.address()
        let urlDisplay = "null"

        if (typeof addrInfo === "string") {
            urlDisplay = addrInfo
        } else if (addrInfo) {
            const hostname =
                addrInfo.address === "::" ? "localhost" : addrInfo.address
            urlDisplay = `http://${hostname}:${addrInfo.port}`
        }

        console.log(`=> Server running at ${urlDisplay}`)
    })
}

main()
