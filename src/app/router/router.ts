import express from "express"

import {
    authenticate,
    deauthenticate,
    useTokenAuthentication,
} from "../auth.js"
import dbp from "../db.js"
import { asString } from "../utils.js"
import accessControlRoutes from "./accessControl.js"
import apiRoutes from "./api.js"
import profileRoutes from "./profile.js"
import usersRoutes from "./users.js"

const router = express.Router()

router.post("/signin", authenticate)
router.get("/signout", deauthenticate)

// All routes after this will use token authentication, ie. populate req.user if signed in
router.use(useTokenAuthentication())

router.get("/", (req, res) => {
    res.render("index")
})

router.get("/signin", async (req, res) => {
    const db = await dbp
    const mezzoDomain = db.data.get("domain").value()
    const url = asString(req.query.url)

    try {
        if (url) {
            const domain = new URL(url).hostname

            // prevent redirecting to a different domain
            if (domain !== mezzoDomain && !domain.endsWith(`.${mezzoDomain}`)) {
                await req.flash("warning", `Invalid URL: ${url}`)
                res.redirect("/signin")
                return
            }
        }

        if (req.user) {
            res.redirect(url || "/")
            return
        }

        res.render("signin", { url })
    } catch (e) {
        res.redirect("/signin")
    }
})

router.use("/profile", profileRoutes)
router.use("/users", usersRoutes)
router.use("/access_control", accessControlRoutes)
router.use("/api", apiRoutes)

// catch-alls
router.use((req, res) => {
    res.status(404)
    res.render("404")
})

router.use(((err, req, res, next) => {
    console.log("Server error:", err)

    res.status(500)
    res.render("500")
}) as express.ErrorRequestHandler)

export default router
