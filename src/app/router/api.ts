import express from "express"

import { authorizeUser, revokeToken } from "../auth.js"
import dbp from "../db.js"
import { asString } from "../utils.js"

const router = express.Router()

// redirect on not authorized, otherwise set a header to the user profile
router.get("/verify", async (req, res) => {
    const db = await dbp
    const mezzoUrl = new URL(db.data.get("mezzo_url").value())
    const mezzoDomain = db.data.get("domain").value()

    const check = asString(req.query.url)
    const protocol = req.header("x-forwarded-proto") || "https"
    const host = req.header("x-forwarded-host") || ""
    const uri = req.header("x-forwarded-uri") || "/"
    const forwarded = check || `${protocol}://${host}${uri}`

    if (!req.user) {
        mezzoUrl.pathname = "/signin"
        mezzoUrl.searchParams.set("url", forwarded)
        res.status(401).redirect(mezzoUrl.toString())
        return
    }

    try {
        const domain = new URL(forwarded).hostname

        // prevent redirecting to a different domain
        if (domain !== mezzoDomain && !domain.endsWith(`.${mezzoDomain}`)) {
            await req.flash("warning", `Invalid URL: ${forwarded}`)
            res.status(400).redirect(mezzoUrl.toString())
            return
        }

        if (!(await authorizeUser(req.user.username, domain))) {
            await req.flash(
                "warning",
                `You don't have access to ${forwarded || "that site"}`,
            )
            res.status(403).redirect(mezzoUrl.toString())
            return
        }
    } catch (e) {
        await req.flash("warning", `Invalid URL: ${forwarded}`)
        res.status(400).redirect(mezzoUrl.toString())
        return
    }

    res.set("X-Remote-User", req.user.username)
    res.set("X-Remote-Groups", req.user.roles.join(","))

    res.status(200).end()
})

// all the routes below require an authenticated user
router.use((req, res, next) => {
    if (!req.user) {
        res.status(401).json({ status: 401 })
        return
    }

    next()
})

router.get("/me", (req, res) => {
    const user = req.user!
    res.json({ status: 200, user })
})

router.delete("/token/:key", async (req, res) => {
    if (!(await revokeToken(req.user!.username, req.params.key))) {
        res.status(404).json({ status: 404, error: "Token not found" })
        return
    }

    res.json({ status: 200 })
})

// catch-alls
router.use((req, res) => {
    res.status(404).json({ status: 404, error: "API endpoint not found" })
})

router.use(((err, req, res, next) => {
    console.log("Server API error:", err)

    res.status(500).json({ status: 500 })
}) as express.ErrorRequestHandler)

export default router
