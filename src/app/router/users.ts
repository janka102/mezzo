import express from "express"

import {
    changeUserPassword,
    requireAuth,
    revokeToken,
    toDateLocaleString,
    updateUserSettings,
} from "../auth.js"
import dbp, { type User } from "../db.js"
import { asString, getValidTimeZones } from "../utils.js"

const router = express.Router()

// all these routes require admin role
router.use(requireAuth(null, "admin"))

router.get("/", async (req, res) => {
    const open = asString(req.query.open)
    const validTimeZones = getValidTimeZones()
    const db = await dbp
    const users = db.data
        .get("users")
        .map(({ password, ...user }) => {
            const mappedUser = {
                ...user,
                numTokens: db.data
                    .get("tokens")
                    .filter({ username: user.username })
                    .size()
                    .value(),
            }

            return mappedUser
        })
        .value()

    res.render("users", { users, open, validTimeZones })
})

router.post("/", async (req, res) => {
    const username = asString(req.body.username)
    const db = await dbp
    let error = true

    if (!username.match(/^[0-9A-Za-z_-]+$/)) {
        await req.flash(
            "danger",
            "Invalid username, must be 0-9, A-Z, a-z, _, and - only",
        )
    } else if (db.data.get("users").has(username).value()) {
        await req.flash("danger", "Username already taken")
    } else {
        db.data.set(["users", username], { username, roles: [] }).value()

        if (!(await updateUserSettings(username, req))) {
            db.data.unset(["users", username]).value()
            await db.write()
        } else if (!(await changeUserPassword(username, req))) {
            db.data.unset(["users", username]).value()
            await db.write()
        } else {
            await req.flash("success", "Successfully created new user")
            error = false
        }
    }

    res.redirect("/users" + (error ? "?open=new-user" : ""))
})

router.get("/:username", async (req, res) => {
    const { username } = req.params
    const open = asString(req.query.open)
    const db = await dbp
    const rawUser: User | undefined = db.data.get(["users", username]).value()

    if (!rawUser) {
        res.status(404).render("user-edit", { editing: null })
        return
    }

    const { password, ...user } = rawUser
    const now = Date.now()
    const validTimeZones = getValidTimeZones()
    const tokens = db.data
        .get("tokens")
        .flatMap<{
            username: string
            userAgent: string
            key: string
            created: string
            expires: string
        }>(({ value, created, expires, ...token }) =>
            token.username === username && expires > now
                ? {
                      ...token,
                      created: toDateLocaleString(created, user.timeZone),
                      expires: toDateLocaleString(expires, user.timeZone),
                  }
                : [],
        )
        .value()

    res.render("user-edit", {
        editing: { ...user, tokens },
        open,
        validTimeZones,
    })
})

router.post("/:username/revoke-token", async (req, res) => {
    const { username } = req.params
    const key = asString(req.body.key)

    if (await revokeToken(username, key)) {
        await req.flash("success", "Successfully revoked user's token")
    } else {
        await req.flash("warning", "Couldn't find user's token")
    }

    res.redirect(`/users/${username}`)
})

router.post("/:username/settings", async (req, res) => {
    const { username } = req.params

    if (await updateUserSettings(username, req)) {
        await req.flash("success", "Successfully updated user's settings")
    }

    res.redirect(`/users/${username}`)
})

router.post("/:username/password", async (req, res) => {
    const { username } = req.params

    if (await changeUserPassword(username, req)) {
        await req.flash("success", "Successfully changed user's password")
        res.redirect(`/users/${username}`)
    } else {
        res.redirect(`/users/${username}?open=password`)
    }
})

router.post("/:username/delete", async (req, res) => {
    const db = await dbp
    const { username } = req.params

    if (username === req.user!.username) {
        await req.flash("danger", "Can't delete your own user")
    } else if (!db.data.get("users").has(username).value()) {
        await req.flash("danger", "User not found")
    } else {
        await req.flash("success", "Successfully deleted user", true)
        db.data.unset(["users", username]).value()
        db.data.unset(["flashes", username]).value()
        await db.write()
    }

    res.redirect("/users")
})

export default router
