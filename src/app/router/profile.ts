import express from "express"

import {
    changeUserPassword,
    requireAuth,
    revokeToken,
    toDateLocaleString,
    updateUserSettings,
} from "../auth.js"
import dbp from "../db.js"
import { asString, getValidTimeZones } from "../utils.js"

const router = express.Router()

// all these routes require signed in user
router.use(requireAuth("/profile", null))

router.get("/", async (req, res, next) => {
    const open = asString(req.query.open).toLowerCase()
    const { username, timeZone } = req.user!
    const now = Date.now()
    const validTimeZones = getValidTimeZones()

    // get all tokens for the current user only
    const db = await dbp
    const tokens = db.data
        .get("tokens")
        .reduce<
            {
                username: string
                userAgent: string
                key: string
                created: string
                expires: string
            }[]
        >((tokens, { value, created, expires, ...token }) => {
            if (token.username === username && expires > now) {
                tokens.push({
                    ...token,
                    created: toDateLocaleString(created, timeZone),
                    expires: toDateLocaleString(expires, timeZone),
                })
            }

            return tokens
        }, [])
        .value()

    res.render("profile", { tokens, open, validTimeZones })
})

router.post("/revoke-token", async (req, res) => {
    if (await revokeToken(req.user!.username, asString(req.body.key))) {
        await req.flash("success", "Successfully revoked token")
    } else {
        await req.flash("warning", "Couldn't find token")
    }

    res.redirect("/profile")
})

router.post("/settings", async (req, res) => {
    if (await updateUserSettings(req.user!.username, req)) {
        await req.flash("success", "Successfully updated settings")
        res.redirect("/profile")
    } else {
        res.redirect("/profile?open=settings")
    }
})

router.post("/password", async (req, res) => {
    if (await changeUserPassword(req.user!.username, req)) {
        await req.flash("success", "Successfully changed password")
        res.redirect("/profile")
    } else {
        res.redirect("/profile?open=password")
    }
})

export default router
