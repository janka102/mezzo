import express from "express"

import { requireAuth, updateAccessControl } from "../auth.js"
import dbp, { type AccessControl } from "../db.js"
import { asString } from "../utils.js"

const router = express.Router()

// all these routes require admin role
router.use(requireAuth(null, "admin"))

router.get("/", async (req, res) => {
    const db = await dbp
    const accessControl = db.data
        .get("access_control")
        .map((ctrl, domain) => {
            const mappedCtrl: AccessControl & { domain: string } = {
                ...ctrl,
                domain,
            }

            return mappedCtrl
        })
        .sortBy(["domain"])
        .value()

    res.render("access-control", {
        access_control: accessControl,
        domain: db.data.get("domain").value(),
    })
})

router.get("/:domain", async (req, res) => {
    const domain = req.params.domain.toLowerCase()
    const db = await dbp
    const accessControl = db.data.get(["access_control", domain]).value()

    if (!accessControl) {
        res.render("access-control-edit", {
            editing: null,
            domain: db.data.get("domain").value(),
        })
    } else {
        res.render("access-control-edit", {
            editing: {
                ...accessControl,
                domain,
            },
            domain: db.data.get("domain").value(),
        })
    }
})

router.post("/", async (req, res) => {
    const domain = asString(req.body.domain).toLowerCase()
    const type = asString(req.body.type)
    const roles = asString(req.body.roles)
    const db = await dbp

    /** @see https://datatracker.ietf.org/doc/html/rfc1035#section-2.3.4 */
    if (
        !domain ||
        !domain.match(
            /^([a-z0-9]([a-z0-9-]{0,64}[a-z0-9])?\.){1,128}[a-z0-9]{2,64}$/,
        )
    ) {
        await req.flash("danger", "Invalid domain")
    } else if (db.data.get(["access_control", domain]).value()) {
        await req.flash("danger", "A rule already exists for this domain")
    } else {
        db.data.set(["access_control", domain], { type, roles }).value()

        if (!(await updateAccessControl(domain, req))) {
            db.data.unset(["access_control", domain]).value()
            await db.write()
        } else {
            await req.flash("success", "Successfully created new rule")
        }
    }

    res.redirect("/access_control")
})

router.post("/:domain", async (req, res) => {
    const domain = req.params.domain.toLowerCase()
    const db = await dbp

    if (!db.data.get(["access_control", domain]).value()) {
        await req.flash("danger", "Domain not found")
    } else if (await updateAccessControl(domain, req)) {
        await req.flash("success", "Successfully updated rule")
    }

    res.redirect("/access_control")
})

router.post("/:domain/delete", async (req, res) => {
    const domain = req.params.domain.toLowerCase()
    const db = await dbp
    const accessControl = db.data.get(["access_control", domain]).value()

    if (!accessControl) {
        await req.flash("danger", "No rule found")
    } else {
        db.data.unset(["access_control", domain]).value()
        await db.write()
    }

    res.redirect("/access_control")
})

export default router
