import argon2 from "argon2"
import crypto from "crypto"
import type express from "express"
import { promisify } from "util"

import dbp, { type AccessControl, type Token, type User } from "./db.js"
import { asString } from "./utils.js"

const randomBytes = promisify(crypto.randomBytes)
const development =
    !process.env.NODE_ENV || process.env.NODE_ENV === "development"

export type AuthUser = Omit<User, "password"> & {
    token: Omit<Token, "value">
}

declare global {
    namespace Express {
        interface Request {
            user?: AuthUser
        }

        interface Locals {
            user?: AuthUser
        }
    }
}

const TOKEN_NAME = "token"
const TOKEN_SIZE = 32
const AUTHORIZATION_PREFIX = "Bearer "

/**
 * Authenticate a sign in request.
 */
async function authenticate(req: express.Request, res: express.Response) {
    const url = asString(req.query.url)

    const db = await dbp
    const mezzoUrl = db.data.get("mezzo_url").value()
    const mezzoDomain = db.data.get("domain").value()
    const expiresIn = asString(req.body["expires-in"])
    const username = await validateCredentials(req).catch(() => null)

    // if invalid sign in credentials, send back to sign in page
    if (!username) {
        await req.flash("danger", "Invalid sign in credentials")
        res.status(401)
        res.render("signin", { url })
        return
    }

    if (url) {
        try {
            const domain = new URL(url).hostname

            // prevent redirecting to a different domain
            if (domain !== mezzoDomain && !domain.endsWith(`.${mezzoDomain}`)) {
                await req.flash("warning", `Invalid URL: ${url}`)
                res.redirect("/signin")
                return
            }
        } catch (e) {
            await req.flash("warning", `Invalid URL: ${url}`)
            res.redirect("/signin")
            return
        }
    }

    const userAgent = req.header("user-agent") || ""
    const domain = db.data.get("domain").value()
    const token = await generateToken(
        username,
        userAgent,
        Number(expiresIn) * 1000,
    )

    res.cookie(TOKEN_NAME, token.value, {
        domain,
        httpOnly: true,
        secure: !development,
        sameSite: "lax",
        expires: new Date(token.expires),
    })
    res.redirect(url || mezzoUrl)
}

/**
 * Process a sign out request.
 */
async function deauthenticate(req: express.Request, res: express.Response) {
    const tokenValue = getToken(req)

    if (tokenValue) {
        const db = await dbp
        db.data.unset(["tokens", tokenValue]).value()
        await db.write()
    }

    clearToken(req, res)

    res.redirect("/")
}

/**
 * Check if there is a valid token and populate `req.user` and `res.locals.user`.
 */
function useTokenAuthentication(): express.RequestHandler {
    return async (req, res, next) => {
        const token = await validateToken(req, res)

        // no token, not authenticated
        if (!token) {
            next()
            return
        }

        const db = await dbp
        const user = db.data
            .get(["users", token.username])
            .omit("password")
            .set("token", token)
            .value() as AuthUser // TypeScript doesn't know that the token is set

        req.user = user
        res.locals.user = user

        next()
    }
}

/**
 * Require this path to be authenticated. Redirects to sign in page otherwise.
 *
 * @param redirect Optional redirect url, defaults to failed path
 * @param role Optional role required for the user
 */
function requireAuth(
    redirect: string | null,
    role: string | null,
): express.RequestHandler {
    return async (req, res, next) => {
        if (!req.user) {
            const url = redirect || req.originalUrl

            res.status(401)
            res.redirect(`/signin?url=${encodeURIComponent(url)}`)
            return
        }

        if (role && !req.user.roles.includes(role)) {
            const url = redirect || "/"

            await req.flash(
                "danger",
                `Only users with role '${role}' can view that page`,
            )

            res.status(403)
            res.redirect(url)
            return
        }

        next()
    }
}

/**
 * Create an authentication token and save it to the database.
 *
 * @param username Username of the user to create a token for
 * @param userAgent User agent of the requesting client
 * @param expiresIn Number of milliseconds until this token expires
 *
 * @return The generated token for the given user
 */
async function generateToken(
    username: string,
    userAgent: string,
    expiresIn: number,
): Promise<Token> {
    const db = await dbp

    const now = Date.now()
    const value = (await randomBytes(TOKEN_SIZE)).toString("hex")

    const tokens = db.data.get("tokens").value()

    // throw error if duplicate token
    // probability of this is super low, but check just in case
    if (value in tokens) {
        const numTokens = Object.keys(tokens).length
        console.warn(`generateToken duplicate ID with ${numTokens} tokens`)
        throw new Error("Duplicate token generated")
    }

    // default is 1 hour, maximum is 4 weeks
    expiresIn = Math.min(
        expiresIn || 60 * 60 * 1000,
        4 * 7 * 24 * 60 * 60 * 1000,
    )

    // token key needs to be unique per _user_
    // here: find max existing key for user and add 1
    const maxKey = Object.values(tokens).reduce((maxKey, token) => {
        const tokenKey = Number(token.key)

        if (token.username === username && tokenKey > maxKey) {
            return tokenKey
        }

        return maxKey
    }, 0)

    const token: Token = {
        value,
        username,
        userAgent,
        key: (maxKey + 1).toString(),
        created: now,
        expires: now + expiresIn,
    }

    db.data.set(["tokens", token.value], token).value()
    await db.write()

    return token
}

/**
 * Retrieve a token from a request.
 *
 * @returns The token value from the request
 */
function getToken(req: express.Request): string | null {
    if (req.cookies?.[TOKEN_NAME]) {
        return req.cookies[TOKEN_NAME]
    }

    const auth = req.header("Authorization")
    if (auth?.startsWith(AUTHORIZATION_PREFIX)) {
        return auth.slice(AUTHORIZATION_PREFIX.length)
    }

    return null
}

/**
 * Clear a token from the request.
 */
function clearToken(req: express.Request, res: express.Response) {
    if (req.cookies?.[TOKEN_NAME]) {
        res.clearCookie(TOKEN_NAME)
    }
}

/**
 * Validate and retrieve a token on a request.
 *
 * @returns The user token, or null if not validated
 */
async function validateToken(
    req: express.Request,
    res: express.Response,
): Promise<Omit<Token, "value"> | null> {
    const tokenValue = getToken(req)

    if (!tokenValue) {
        return null
    }

    const db = await dbp
    const now = Date.now()
    const rawToken: Token | undefined = db.data
        .get(["tokens", tokenValue])
        .value()

    if (!rawToken || rawToken.expires <= now) {
        clearToken(req, res)
        return null
    }

    const { value, ...token } = rawToken

    return token
}

/**
 * Check if the user is authorized to access the domain.
 *
 * @returns If the user is authorized
 */
async function authorizeUser(
    username: string,
    domain: string,
): Promise<boolean> {
    const db = await dbp
    const user = db.data.get(["users", username]).value()
    const rule = db.data
        .get(
            ["access_control", domain.toLowerCase()],
            db.data.get(["access_control", "*"]).value(),
        )
        .value()

    // do some sanity checks just to make sure everything is working
    if (!domain || !user || !rule) {
        return false
    }

    const hasRole = user.roles.some((role) => rule.roles.includes(role))

    return (
        (rule.type === "allow" && hasRole) || (rule.type === "deny" && !hasRole)
    )
}

/**
 * Validate user credentials from a request.
 *
 * @returns The username of the user, or null if not validated
 */
async function validateCredentials(
    req: express.Request,
): Promise<string | null> {
    const db = await dbp
    const username = asString(req.body.username)
    const password = asString(req.body.password)

    if (!username || !password) {
        return null
    }

    const user = db.data.get(["users", username]).value()

    if (!user || !(await argon2.verify(user.password, password))) {
        return null
    }

    return username
}

/**
 * Remove a token from the database.
 *
 * @param username Username of the user token to remove
 * @param tokenKey Token key to remove
 *
 * @returns If the token was found and deleted
 */
async function revokeToken(
    username: string,
    tokenKey: string,
): Promise<boolean> {
    const db = await dbp
    const token = db.data
        .get("tokens")
        .find({ username, key: tokenKey })
        .value()

    if (token) {
        db.data.unset(["tokens", token.value]).value()
        await db.write()
    }

    return !!token
}

/**
 * @returns If the update completed without error
 */
async function updateUserSettings(
    username: string,
    req: express.Request,
): Promise<boolean | null> {
    const db = await dbp

    if (!req.user) {
        await req.flash("danger", "User not found")
        return null
    }

    const user = db.data.get(["users", username]).value()
    if (!user) {
        await req.flash("danger", "User not found")
        return null
    }

    const timeZone = asString(req.body["time-zone"])
    const userRoles = asString(req.body["user-roles"])
    const errors = []
    const update: Partial<Pick<User, "roles" | "timeZone">> = {}

    // only admins can change roles
    if (req.user.roles.includes("admin")) {
        if (typeof userRoles !== "string") {
            errors.push("Invalid value for `user-roles`.")
        } else {
            // convert to lowercase, remove empty strings, remove duplicates, and sort
            update.roles = Array.from(
                new Set(
                    userRoles
                        .split(",")
                        .flatMap((role) => role.trim().toLowerCase() || []),
                ),
            ).sort()

            // check if trying to remove the only admin
            if (
                user.roles.includes("admin") &&
                !update.roles.includes("admin")
            ) {
                // find any other admin
                const otherAdmin: User | undefined = db.data
                    .get("users")
                    .find(
                        ({ roles, username: filterUsername }) =>
                            filterUsername !== username &&
                            roles.includes("admin"),
                    )
                    .value()

                if (!otherAdmin) {
                    errors.push("Cannot remove the only admin")
                    update.roles.unshift("admin")
                    update.roles.sort()
                }
            }
        }
    }

    if (typeof timeZone !== "string") {
        errors.push("Invalid value for `time-zone`.")
    } else {
        try {
            // check if time zone is valid
            new Intl.DateTimeFormat("en-US", { timeZone })

            update.timeZone = timeZone
        } catch (e) {
            errors.push("Invalid value for `time-zone`.")
        }
    }
    await req.flash("warning", errors, true)

    db.data.get(["users", username]).assign(update).value()
    await db.write()

    return errors.length === 0
}

/**
 * @returns If the change completed without error
 */
async function changeUserPassword(
    username: string,
    req: express.Request,
): Promise<boolean | null> {
    const db = await dbp
    const user = db.data.get(["users", username]).value()

    if (!user) {
        await req.flash("danger", "User not found")
        return null
    }

    const password = asString(req.body.password)
    const repeatPassword = asString(req.body["password-repeat"])
    const currentPassword = asString(req.body["current-password"])
    const errors = []

    if (req.user?.username === username) {
        if (!currentPassword) {
            errors.push("Current password is required")
        } else if (!(await argon2.verify(user.password, currentPassword))) {
            errors.push("Current password is incorrect")
        }
    }

    if (typeof password !== "string" || password.length < 8) {
        errors.push("New password too short")
    }

    if (password !== repeatPassword) {
        errors.push("Passwords don't match")
    }

    if (errors.length) {
        await req.flash("danger", errors, true)
        return false
    }

    // now we know password length is >= 8 and matches the repeated one
    const update: Pick<User, "password"> = {
        password: await argon2.hash(password),
    }

    db.data.get(["users", username]).assign(update).value()
    await db.write()

    return errors.length === 0
}

/**
 * @returns If the update completed without error
 */
async function updateAccessControl(
    domain: string,
    req: express.Request,
): Promise<boolean | null> {
    const db = await dbp
    const accessControl = db.data
        .get(["access_control", domain.toLowerCase()])
        .value()

    if (!accessControl) {
        await req.flash("danger", "Access control rule not found")
        return null
    }

    const type = asString(req.body.type)
    const roles = asString(req.body.roles)
    const errors = []

    if (type !== "allow" && type !== "deny") {
        errors.push("Invalid value for `type`.")
    }

    if (typeof roles !== "string") {
        errors.push("Invalid value for `roles`.")
    }

    if (errors.length > 0) {
        await req.flash("danger", errors, true)
        return false
    }

    const update: AccessControl = {
        type: type as "allow" | "deny", // checked above
        roles: Array.from(
            new Set(
                (roles as string) // checked above
                    .split(",")
                    .flatMap((role) => role.trim().toLowerCase() || []),
            ),
        ).sort(),
    }

    db.data.get(["access_control", domain.toLowerCase()]).assign(update).value()
    await db.write()

    return true
}

/**
 * @param time Unix epoch timestamp in milliseconds
 * @param timeZone example: America/Los_Angeles
 *
 * @returns The time as a localized string
 */
function toDateLocaleString(time: number, timeZone: string): string {
    try {
        return new Date(time).toLocaleString("en-US", {
            timeZone,
            timeZoneName: "short",
        })
    } catch (e) {
        if (timeZone !== "UTC") {
            // switch to UTC time zone and try again
            return toDateLocaleString(time, "UTC")
        }

        throw e
    }
}

export {
    authenticate,
    authorizeUser,
    changeUserPassword,
    deauthenticate,
    generateToken,
    requireAuth,
    revokeToken,
    toDateLocaleString,
    updateAccessControl,
    updateUserSettings,
    useTokenAuthentication,
    validateCredentials,
}
