import { type Handler } from "express"
import dbp, { FlashType, type Flash } from "./db.js"

declare global {
    namespace Express {
        interface Request {
            /**
             * Read or write flash messages of a certain type.
             * When reading, also clears the data from the database at the same time.
             *
             * @param type The type of messages to save or retrieve
             * @param msg The message to save
             * @param skipWrite If this operation should skip writing to the database immediately
             *
             * @returns The messages retrieved from the data if it is a read
             */
            flash(type?: null, msg?: null, skipWrite?: boolean): Promise<Flash>
            flash(
                type: `${FlashType}`,
                msg?: null,
                skipWrite?: boolean,
            ): Promise<string[]>
            flash(
                type: `${FlashType}`,
                msg: string | string[],
                skipWrite?: boolean,
            ): Promise<void>
        }

        interface Locals {
            flash(): Promise<Flash>
            flash(type: `${FlashType}`): Promise<string[]>
        }
    }
}

/**
 * Adds flash message to locals. Saves messages so when the next page loads it can read those messages.
 * If there is a request user then save the messages to the database, otherwise save to an anonymous object in memory.
 */
export const flash: () => Handler = () => {
    let anonFlash: Flash = {}

    return async (req, res, next) => {
        const db = await dbp

        async function flash(
            type?: null,
            msg?: null,
            skipWrite?: boolean,
        ): Promise<Flash>
        async function flash(
            type: `${FlashType}`,
            msg?: null,
            skipWrite?: boolean,
        ): Promise<string[]>
        async function flash(
            type: `${FlashType}`,
            msg: string | string[],
            skipWrite: boolean,
        ): Promise<void>
        async function flash(
            type?: `${FlashType}` | null,
            msg?: string | string[] | null,
            skipWrite = false,
        ): Promise<Flash | string[] | void> {
            if (!req.user) {
                if (!type) {
                    // get and clear all messages
                    const flash = anonFlash
                    anonFlash = {}
                    return flash
                }

                if (!msg) {
                    // get and clear this type only
                    const flash = anonFlash[type] || []
                    delete anonFlash[type]
                    return flash
                }

                // append message(s) to the type
                anonFlash[type] = (anonFlash[type] || []).concat(msg)
                return
            }

            if (!type) {
                // get and clear all messages
                const flash =
                    db.data.get(["flashes", req.user.username]).value() || {}
                db.data.unset(["flashes", req.user.username]).value()

                if (!skipWrite) {
                    await db.write()
                }

                return flash
            }

            if (!msg) {
                // get and clear this type only
                const flash =
                    db.data.get(["flashes", req.user.username, type]).value() ||
                    []
                db.data.unset(["flashes", req.user.username, type]).value()

                if (!skipWrite) {
                    await db.write()
                }

                return flash
            }

            // append message(s) to the type
            db.data
                .set(
                    ["flashes", req.user.username, type],
                    (
                        db.data
                            .get(["flashes", req.user.username, type])
                            .value() || []
                    ).concat(msg),
                )
                .value()

            if (!skipWrite) {
                await db.write()
            }

            return
        }

        function readOnlyFlash(type?: null): Promise<Flash>
        function readOnlyFlash(type: `${FlashType}`): Promise<string[]>
        function readOnlyFlash(
            type?: `${FlashType}` | null,
        ): Promise<Flash | string[]> {
            return type ? flash(type) : flash()
        }

        req.flash = flash

        // locals flash can only read messages
        res.locals.flash = readOnlyFlash

        next()
    }
}
